package com.darwingtechnologies.android.appname.fragments.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

@SuppressLint("ValidFragment")
public class ExitDialogFragment extends BottomSheetDialogFragment {
    private View customView;
    private Context context;
    private TextView tvExit, tvCancel;


    public ExitDialogFragment() {}

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        context = getContext();
        customView = CommonMethods.createView(context, R.layout.exit_dialog_fragment, null);

        final Dialog dialog = new Dialog(context, R.style.DialogFragmentThemes);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        tvExit= customView.findViewById(R.id.tvExit);
        tvCancel= customView.findViewById(R.id.tvCancel);

        tvExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                try {
                    getActivity().finishAffinity();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        customView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dismiss();
                    }
                });

        customView.setFocusableInTouchMode(true);
        customView.requestFocus();
        customView.setOnKeyListener(
                new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                dismiss();
                                return true;
                            }
                        }
                        return false;
                    }
                });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(customView);
        return dialog;
    }

}
