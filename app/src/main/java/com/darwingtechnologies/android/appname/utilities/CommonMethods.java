package com.darwingtechnologies.android.appname.utilities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.activities.MainActivity;
import com.darwingtechnologies.android.appname.fragments.BaseFragment;


import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.List;

public class CommonMethods {

    private static ProgressDialog pgDialog;
    private static Toast toast;

    public static void showProgressDialog(Context nContext) {

        try {
            if (pgDialog != null) {
                pgDialog.hide();
            }
            pgDialog = new ProgressDialog(nContext);
            if (pgDialog.getWindow() != null)
                pgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pgDialog.setIndeterminate(true);
            pgDialog.setCancelable(false);
            pgDialog.show();
            pgDialog.setContentView(R.layout.loti_loading);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void lotiProgressLoading(Context nContext,String type ) {

        try {
            if (pgDialog != null) {
                pgDialog.hide();
            }
            pgDialog = new ProgressDialog(nContext);
            if (pgDialog.getWindow() != null)
                pgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pgDialog.setIndeterminate(true);
            pgDialog.setCancelable(false);
            pgDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static android.app.AlertDialog.Builder displayMessagesDialog(final String message, final Context context) {

        final android.app.AlertDialog.Builder builder =
                new android.app.AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        builder.create().hide();
                    }
                });

        return builder;
    }




    public static void hideProgressDialog() {
        try {
            if (pgDialog != null) {
                pgDialog.dismiss();
            }
            if (MainActivity.isInstanciated()) {
                MainActivity.instance().isShowSpinKit(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void callFragmentWithParameter(
            Fragment nFragment,
            int view,
            Bundle bundle,
            int enterAnim,
            int exitAnim,
            Context context) {
        FragmentManager fm;
        nFragment.setArguments(bundle);
        fm = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(enterAnim, exitAnim);
        fragmentTransaction.replace(view, nFragment);
        fragmentTransaction.commit();
    }

    public static void callFragment(
            Fragment nFragment,
            int view,
            int enterAnim,
            int exitAnim,
            int popEnterAnim,
            int popExitAnim,
            Context context,
            boolean isBack) {
        FragmentManager fm;
        fm = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        fragmentTransaction.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim);
        fragmentTransaction.replace(view, nFragment);
        if (isBack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    public static String getExtensionFromFileName(String fileName) {
        String extension = null;
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }

    public static void showToast(String message, int toastDuration) {
        try {
            cancelToast();
            //            View v = createView(CommonObjects.getContext(), R.layout.toast_layout,
            // null);
            //            TextView nTextView = (TextView) v.findViewById(R.id.tvToast);
            //            nTextView.setText(message);
            toast = Toast.makeText(CommonObjects.getContext(), message, toastDuration);
            toast.setGravity(Gravity.CENTER, 0, 0);
            //            toast.setView(v);
            toast.show();
        } catch (Exception e) {
        }
    }

    public static void cancelToast() {
        try {
            if (toast != null) {
                toast.cancel();
            }
        } catch (Exception e) {
        }
    }

    public static void hideSoftKeyboard(View v, Context context) {
        try {
            InputMethodManager imm =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager)
                        CommonObjects.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (imm != null && view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Boolean isKeyboardShowing() {
        InputMethodManager imm =
                (InputMethodManager)
                        CommonObjects.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            //            writeToLog("Software Keyboard was shown");
            return true;
        } else {
            //            writeToLog("Software Keyboard was not shown");
            return false;
        }
    }

    public static View createView(Context context, int layout, ViewGroup parent) {
        try {
            LayoutInflater newLayoutInflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            return newLayoutInflater.inflate(layout, parent, false);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getStringPreference(
            Context nContext, String preferenceName, String preferenceItem, String defaultValue) {
        try {
            //            if (CommonObjects.getSignInResponse() != null) {
            //                preferenceName = preferenceName + "_" +
            // CommonObjects.getSignInResponse().getData().getUserName().getId();
            //            }
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            return nPreferences.getString(preferenceItem, defaultValue);
        } catch (Exception e) {
            return "";
        }
    }

    public static int getIntPreference(
            Context nContext, String preferenceName, String preferenceItem, int deafaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            return nPreferences.getInt(preferenceItem, deafaultValue);
        } catch (Exception e) {
            return deafaultValue;
        }
    }

    public static Boolean getBooleanPreference(
            Context nContext, String preferenceName, String preferenceItem, Boolean defaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            return nPreferences.getBoolean(preferenceItem, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static void setStringPreference(
            Context nContext,
            String preferenceName,
            String preferenceItem,
            String preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putString(preferenceItem, preferenceItemValue);
            nEditor.apply();
        } catch (Exception e) {
        }
    }

    public static void clearPreferences(FragmentActivity fragmentActivity,
                                        String preferenceFileName) {
        try {
            SharedPreferences nPreferences;
            nPreferences = fragmentActivity.getSharedPreferences(preferenceFileName, Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            if (nEditor != null) {
                try {
                    nEditor.clear().apply();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void setIntPreference(
            Context nContext,
            String preferenceName,
            String preferenceItem,
            int preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putInt(preferenceItem, preferenceItemValue);
            nEditor.apply();
        } catch (Exception e) {
        }
    }



    public static void setBooleanPreference(
            Context nContext,
            String preferenceName,
            String preferenceItem,
            Boolean preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putBoolean(preferenceItem, preferenceItemValue);
            nEditor.apply();
        } catch (Exception e) {
        }
    }


    public static String removeNonDigits(String text) {
        try {
            int length = text.length();
            StringBuffer buffer = new StringBuffer(length);
            for (int i = 0; i < length; i++) {
                char ch = text.charAt(i);
                if (Character.isDigit(ch)) {
                    buffer.append(ch);
                }
            }
            return buffer.toString();
        } catch (Exception e) {
            return text;
        }
    }


    public static boolean isNetworkAvailable(Context nContext) {
        try {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) nContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isWifiConnected(Context nContext) {
        try {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) nContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = null;
            if (connectivityManager != null) {
                networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            }
            return networkInfo == null ? false : networkInfo.isConnected();
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }



    public static void setupUI(final View view, final Context context) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(
                    new View.OnTouchListener() {

                        public boolean onTouch(View v, MotionEvent event) {
                            hideSoftKeyboard(view, context);
                            return false;
                        }
                    });
        }

        // If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView, context);
            }
        }
    }





    // Open email intent to send email
    public static void composeEmail(
            Context context, String email, String subject, Spanned body, String filePath) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    // Open url in browser
    public static void openUrl(Context context, String url) {
        Uri uri = Uri.parse(url);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
        }
    }

    // Share link on faceBook using deep linking
    public static void faceBookShareIntent(Context context, String urlToShare) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        // intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

        // See if official Facebook app is found
        boolean facebookAppFound = false;
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

        // As fallback, launch sharer.php in a browser
        if (!facebookAppFound) {
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        }

        context.startActivity(intent);
    }

    // Share link on twitter using deep linking
    public static void twitterShareIntent(Context context, String urlToShare) {
        // Create intent using ACTION_VIEW and a normal Twitter url:
        String tweetUrl =
                String.format(
                        "https://twitter.com/intent/tweet?text=%s&url=%s",
                        urlEncode(""), urlEncode(urlToShare));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

        // Narrow down to official Twitter app, if available:
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                intent.setPackage(info.activityInfo.packageName);
            }
        }

        context.startActivity(intent);
    }

    // Share link on whatsapp using deep linking
    public static void whatsAppShareIntent(Context context, String text) {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            context.startActivity(whatsappIntent);
        } catch (ActivityNotFoundException ex) {
        }
    }

    public static void smsShareIntent(Context context, String text) {
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("address", "");
            sendIntent.putExtra("sms_body", text);
            sendIntent.setType("vnd.android-dir/mms-sms");
            context.startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf("Twitter Share", "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    // Share link on pinterest using deep linking
    public static void pinterestShareIntent(Context context, String shareUrl) {
        String mediaUrl = "";
        String description = "";
        String url =
                String.format(
                        "https://www.pinterest.com/pin/create/button/?url=%s&media=%s&description=%s",
                        urlEncode(shareUrl), urlEncode(mediaUrl), description);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        filterByPackageName(context, intent, "com.pinterest");
        context.startActivity(intent);
    }

    // Open instagram url in instagram app ussing deep linking if available otherwise open it in
    // browser
    public static void openInstagram(Context context, String url) {
        Uri uri = Uri.parse(url.replace(".com/", ".com/_u/"));
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            context.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    private static void filterByPackageName(Context context, Intent intent, String prefix) {
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(prefix)) {
                intent.setPackage(info.activityInfo.packageName);
                return;
            }
        }
    }

    // Check for version greater then LOLLIPOP
    public static boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    // Check if specific array of permissions are given or not
    public static boolean checkAllPermission(Context context, String[] permissons) {
        boolean isAllow = false;
        for (String perm : permissons) {
            if (hasPermission(context, perm)) {
                isAllow = true;
            } else {
                isAllow = false;
            }
        }
        return isAllow;
    }

    // Check if specific permission is given or not
    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermission(Context context, String permission) {
        if (canMakeSmores()) {
            return (context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }

    // Show a permission string message on alert dialog
    public static void showPermissionMessage(
            final Context context, String message, final String[] perms) {
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        int permsRequestCode = 200;
                        ((Activity) context).requestPermissions(perms, permsRequestCode);
                    }
                });

        alertDialogBuilder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        dialogInterface.cancel();
                    }
                });
        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    // Show a string message on alert dialog
    public static void showMessage(Context context, String message) {
        try {
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        if (message.equals("")) {
            message = "No error message has received from server.";
        }
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();


            alertDialog.show();
        }catch (Exception e){
        e.printStackTrace();
        }
    }

     // Show keyboard of specific edittext
    public static void showSoftKeyboard(EditText et, Context context) {
        try {
            InputMethodManager imm =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
        }
    }

    // Load a fragment to frame layout with animation
    public static void callFragment(
            BaseFragment nFragment,
            int view,
            int enterAnim,
            int exitAnim,
            Context context,
            boolean isBack) {
        try {
            FragmentActivity mActivity = ((FragmentActivity) context);
            FragmentManager fm = mActivity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            int enterPopAnim, exitPopAnim;
            enterPopAnim = R.anim.slide_in_left;
            exitPopAnim = R.anim.slide_out_right;
            if (enterAnim == 0) {
                enterAnim = R.anim.slide_in_right;
            }
            if (exitAnim == 0) {
                exitAnim = R.anim.slide_out_left;
            }
            if (enterAnim == R.anim.fade_in) {
                enterPopAnim = R.anim.fade_in;
            }
            if (exitAnim == R.anim.fade_out) {
                exitPopAnim = R.anim.fade_out;
            }
            if (enterAnim == -1 && exitAnim == -1) {
                enterAnim = 0;
                exitAnim = 0;
                enterPopAnim = 0;
                exitPopAnim = 0;
            }
            fragmentTransaction.setCustomAnimations(enterAnim, exitAnim, enterPopAnim, exitPopAnim);
            fragmentTransaction.replace(view, nFragment, nFragment.getFragmentTag());
            if (isBack) {
                fragmentTransaction.addToBackStack(nFragment.getFragmentTag());
            }
            //        else
            //        {
            //            if(mActivity.getSupportFragmentManager().getBackStackEntryCount()>0) {
            //                mActivity.getSupportFragmentManager().popBackStack();
            //            }
            //        }
            fragmentTransaction.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void rate(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
        }
    }

}
