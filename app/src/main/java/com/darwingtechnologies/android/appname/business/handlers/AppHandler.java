package com.darwingtechnologies.android.appname.business.handlers;

import android.util.Log;

import com.darwingtechnologies.android.appname.business.models.HomeListItemsResponse;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;
import com.darwingtechnologies.android.appname.utilities.CommonObjects;
import com.google.gson.Gson;

/**
 * Created by Rana Zeshan on 27-Jun-20.
 */


public class AppHandler {
    private static String API_BASE_URL = "https://sarkarinaukri.org.in/API/";

    /////////////API's
    public interface HomeListItemsListener {
        public void onSuccess(HomeListItemsResponse homeListItemsResponse);
        public void onError(String error);
    }
    public static void getItemsList( final HomeListItemsListener homeListItemsListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());

        new CallForServer(API_BASE_URL + "GetList.php", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("listitemsResponse", "" + response);

                        CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        HomeListItemsResponse homeListItemsResponse = new Gson().fromJson(response, HomeListItemsResponse.class);

                        if ( homeListItemsResponse.getSuccess() && homeListItemsResponse.getData()!=null && homeListItemsResponse.getData().getListItems()!=null && homeListItemsResponse.getData().getListItems().size()>0) {
                            homeListItemsListener.onSuccess(homeListItemsResponse);
                        } else {
                            if ( homeListItemsResponse.getErrors()!=null && homeListItemsResponse.getErrors().size() != 0) {
                                homeListItemsListener.onError(homeListItemsResponse.getErrors() + "");
                            }else if(homeListItemsResponse.getData()==null || homeListItemsResponse.getData().getListItems()==null || homeListItemsResponse.getData().getListItems().size()==0){
                                homeListItemsListener.onError("No Items Found");
                            }
                        }
                    }  catch (Exception e) {
                        e.printStackTrace();
                        homeListItemsListener.onError(e.getMessage());
                    }
                } else {
                    homeListItemsListener.onError(response);
                }
            }
        }).callForServerGet();
    }



}