package com.darwingtechnologies.android.appname.business.handlers;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;
import com.darwingtechnologies.android.appname.utilities.CommonObjects;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Rana Zeshan on 27-Jun-20.
 */

public class CallForServer {

    private String url;
    private OnServerResultNotifier onServerResultNotifier;
    private String NO_INTERNET = "No internet connection";
    private static String BASE_URL="";
    private static String API_BASE_URL = BASE_URL;


    public CallForServer(String url, OnServerResultNotifier onServerResultNotifier) {
        this.url = url;
        this.onServerResultNotifier = onServerResultNotifier;
    }

    public interface OnServerResultNotifier {
        public void onServerResultNotifier(boolean isError, String response);
    }

    //Load data from server
    public void callForServerGet() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {
            AndroidNetworking.initialize(CommonObjects.getContext());
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();
            AndroidNetworking.initialize(CommonObjects.getContext(),client);
            ANRequest.GetRequestBuilder getRequestBuilder = AndroidNetworking.get(API_BASE_URL+url);
            Log.d("Service_Response", url);
            getRequestBuilder.addHeaders("Accept", "application/json");
            getRequestBuilder.build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String jsonStr) {
                        onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                    Log.e("JSON",""+jsonStr);
                }

                @Override
                public void onError(ANError error) {
                    String err="Unable to get data from server";
                    if(error.getErrorBody()!=null)
                    {
                        err=error.getErrorBody();
                    }
                    onServerResultNotifier.onServerResultNotifier(true,err);
                    Log.e("Erorr ",""+err.toString());

                }
            });
        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
            Log.e("No Internet ",""+NO_INTERNET.toString());

        }
    }
}
