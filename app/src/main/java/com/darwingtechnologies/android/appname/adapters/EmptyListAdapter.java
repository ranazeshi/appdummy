package com.darwingtechnologies.android.appname.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;

import java.util.ArrayList;


/**
 * Created by Rana Zeshan on 27-Jun-20.
 */
public class EmptyListAdapter extends RecyclerView.Adapter<EmptyListAdapter.ViewHolder> {

    private ArrayList<String> listItems;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private ViewHolder(View view) {
            super(view);
            mView = view;


        }
    }

    public EmptyListAdapter(ArrayList<String> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.item_home_list, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String title = listItems.get(position);

        Log.e("Title a",title);

        try {


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



}

