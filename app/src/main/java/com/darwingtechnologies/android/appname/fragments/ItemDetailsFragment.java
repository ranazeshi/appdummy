package com.darwingtechnologies.android.appname.fragments;

import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.business.models.ListItem;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;

import java.util.concurrent.TimeUnit;

import nl.changer.audiowife.AudioWife;


/**
 * Created by Rana Zeshan on 27-Jun-20.
 */

public class ItemDetailsFragment extends BaseFragment {
    private ListItem item;
    private ImageButton ibFontSize;
    private TextView tvDescription;
    private LinearLayout llAudioView;


    private View mPlayMedia;
    private View mPauseMedia;
    private SeekBar mMediaSeekBar;
    private ProgressBar pbLoading;
    private TextView mRunTime;
    private TextView mTotalTime;
    private AudioWife audioWife = new AudioWife();


    @Override
    protected void initView() {
        ibFontSize=mView.findViewById(R.id.ibFontSize);
        setIbFontSize(ibFontSize);
        ibFontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });

        setIbBack(mView.findViewById(R.id.ibBack),""+item.getTitle());

        tvDescription=mView.findViewById(R.id.tvDescription);
        tvDescription.setText(""+item.getDesc());

        llAudioView=mView.findViewById(R.id.llAudioView);

        if(item.getAudio()!=null && !item.getAudio().isEmpty()) {
            llAudioView.setVisibility(View.VISIBLE);
            setupAudioStreaming();
        }else{
            llAudioView.setVisibility(View.GONE);
        }

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

    }

    public static ItemDetailsFragment newInstance(ListItem item) {
        ItemDetailsFragment fragment = new ItemDetailsFragment();
        fragment.item=item;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.item_details_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return ItemDetailsFragment.class.getSimpleName();
    }

    private void displayPopupWindow(View anchorView) {
        PopupWindow popup = new PopupWindow(mView);
        View layout = getLayoutInflater().inflate(R.layout.popup_menu_for_text_seekbar, null);
        SeekBar sbTextSize=layout.findViewById(R.id.sbTextSize);
        sbTextSize.setProgress((int) tvDescription.getTextSize());
        sbTextSize.setMax(100);
        sbTextSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//                CommonMethods.showToast(""+i,Toast.LENGTH_LONG);
                tvDescription.setTextSize(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
    }

    void setupAudioStreaming(){
        mPlayMedia = mView.findViewById(R.id.play);
        mPauseMedia = mView.findViewById(R.id.pause);
        mMediaSeekBar = (SeekBar) mView.findViewById(R.id.media_seekbar);
        mRunTime = (TextView) mView.findViewById(R.id.run_time);
        mTotalTime = (TextView) mView.findViewById(R.id.total_time);
        pbLoading=mView.findViewById(R.id.pbLoading);

         audioWife.init(mActivity, Uri.parse((String) item.getAudio()), new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        pbLoading.setVisibility(View.GONE);
                        mPlayMedia.setVisibility(View.VISIBLE);
                        mPlayMedia.setEnabled(true);
                        mTotalTime.setVisibility(View.VISIBLE);
                        mTotalTime.setText(String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes((long) mediaPlayer.getDuration()), TimeUnit.MILLISECONDS.toSeconds((long) mediaPlayer.getDuration()) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) mediaPlayer.getDuration()))));


                        audioWife.setSeekBar(mMediaSeekBar);

                        mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                            @Override
                            public void onSeekComplete(MediaPlayer mp) {
                               if(mPlayMedia.getVisibility()==View.GONE && mPauseMedia.getVisibility()==View.VISIBLE) {
                                   mp.start();
                               }
                            }
                        });
                    }
                }).setPlayView(mPlayMedia)
                        .setPauseView(mPauseMedia)
                        .setSeekBar(mMediaSeekBar)
                        .setRuntimeView(mRunTime)
                        .setTotalTimeView(mTotalTime);

                pbLoading.setVisibility(View.VISIBLE);
                mPlayMedia.setEnabled(false);
                mTotalTime.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(audioWife!=null) {
            audioWife.release();
            audioWife.pause();
        }
    }
}
