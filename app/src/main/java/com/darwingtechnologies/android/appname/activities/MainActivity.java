package com.darwingtechnologies.android.appname.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.fragments.SplashFragment;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;
import com.darwingtechnologies.android.appname.utilities.CommonObjects;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  {
    private static MainActivity instance;
    public SpinKitView spin_kit;
    public static FragmentActivity currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spin_kit = findViewById(R.id.spin_kit);

        instance = this;

        CommonObjects.setContext(this);
        goForNextScreen();

    }

    private void goForNextScreen(){
        CommonMethods.callFragment(SplashFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out,this, false);
    }
    public static final boolean isInstanciated() {
        return instance != null;
    }

    public static final MainActivity instance() {
        if (instance != null) return instance;
        throw new RuntimeException("MainActivity not instantiated yet");
    }
    public boolean popBackStack() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
            return true;
        }
        return false;
    }
    public void isShowSpinKit(Boolean isShow) {
        if (isShow) {
            spin_kit.setVisibility(View.VISIBLE);
        } else {
            spin_kit.setVisibility(View.GONE);
        }
    }

}
