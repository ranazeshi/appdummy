package com.darwingtechnologies.android.appname.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.business.models.ListItem;
import com.darwingtechnologies.android.appname.fragments.ItemDetailsFragment;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.ViewHolder> {


    private Context context;
    private ArrayList<ListItem> listItems = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private ImageView ivImage;
        private TextView tvTitle,tvTitleB;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            ivImage = mView.findViewById(R.id.ivImage);
            tvTitle = mView.findViewById(R.id.tvTitle);
            tvTitleB = mView.findViewById(R.id.tvTitleB);


        }
    }

    public HomeListAdapter(ArrayList<ListItem> listItems) {
        this.listItems=listItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (context == null) context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.item_home_list, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ListItem item = listItems.get(position);

        if(item!= null) {
            if (item.getTitle() != null && !item.getTitle().isEmpty()) {
                holder.tvTitle.setText(item.getTitle());
            } else {
                holder.tvTitle.setText("NA");
            }

            if (item.getTitle2() != null && !item.getTitle2().isEmpty()) {
                holder.tvTitleB.setText(item.getTitle2());
            } else {
                holder.tvTitleB.setText("NA");
            }

            if(item.getIcon()!=null){
                Picasso.with(context).load(item.getIcon()).placeholder(R.drawable.place_holder_image_square).error(R.drawable.place_holder_image_square).into(holder.ivImage);
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonMethods.callFragment(ItemDetailsFragment.newInstance(item), R.id.flFragmentOverlay, 0, 0, context, true);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

}
