package com.darwingtechnologies.android.appname.utilities;

import android.content.Context;


public class CommonObjects {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        CommonObjects.context = context;
    }

}
