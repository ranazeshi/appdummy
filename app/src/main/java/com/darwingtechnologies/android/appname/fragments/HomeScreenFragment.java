package com.darwingtechnologies.android.appname.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baoyz.widget.PullRefreshLayout;
import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.adapters.HomeListAdapter;
import com.darwingtechnologies.android.appname.business.handlers.AppHandler;
import com.darwingtechnologies.android.appname.business.models.HomeListItemsResponse;
import com.darwingtechnologies.android.appname.business.models.ListItem;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomMenuButton;

import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


/**
 * Created by Rana Zeshan on 27-Jun-20.
 */

public class HomeScreenFragment extends BaseFragment {

    private ArrayList<ListItem> listItems = new ArrayList<>();

    private RecyclerView rvListItems;
    private FloatingActionButton fabMenu;
    private PullRefreshLayout swipeRefreshLayout;


    private BoomMenuButton bmb;

    @Override
    protected void initView() {

        fabMenu =  mView.findViewById(R.id.fabMenu);

        fabMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(EmptyFragment.newInstance(), R.id.flFragmentOverlay, 0, 0, mActivity, true);
            }
        });

        bmb=mView.findViewById(R.id.bmb);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimary));
        final Integer menu[] = {
                R.drawable.place_holder_image_square, R.drawable.place_holder_image_square
        };
        final Integer menuText[] = {
                R.string.privacy_policy, R.string.rate_us
        };
        final Integer menuSubText[] = {
                R.string.privacy_policy_subtext, R.string.rate_us_subtext
        };
        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            HamButton.Builder builder = new HamButton.Builder().listener(new OnBMClickListener() {
                @Override
                public void onBoomButtonClick(int index) {
                    switch (index) {
                        case 0:
                            CommonMethods.openUrl(mActivity, "http://www.google.com");
                            break;
                        case 1:
                            CommonMethods.rate(mActivity);
                            break;
                    }
                }
            }).normalTextRes(menuText[i])
                    .subNormalTextRes(menuSubText[i])
                    .normalImageRes(menu[i]);
            bmb.addBuilder(builder);
        }

        rvListItems = mView.findViewById(R.id.rvListItems);
        rvListItems.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvListItems.addOnScrollListener(
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (dy < 0 && bmb.getVisibility()==GONE) bmb.setVisibility(VISIBLE);
                        else if (dy > 0 && bmb.getVisibility()==VISIBLE) bmb.setVisibility(GONE);
                    }

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }
                });
        swipeRefreshLayout = mView.findViewById(R.id.swipeRefreshLayout);

        swipeRefreshLayout.setOnRefreshListener(
                new PullRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadListData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });


        loadListData();

        CommonMethods.setupUI(mView,mActivity);

    }

    @Override
    protected void loadData() {

    }

    public static HomeScreenFragment newInstance() {
        HomeScreenFragment fragment = new HomeScreenFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.home_screen_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return HomeScreenFragment.class.getSimpleName();
    }


    @Override
    public void onResume() {
        super.onResume();

    }



    private void loadListData()
    {
        CommonMethods.showProgressDialog(mActivity);
        listItems.clear();

        AppHandler.getItemsList(new AppHandler.HomeListItemsListener() {
            @Override
            public void onSuccess(HomeListItemsResponse homeListItemsResponse) {
                listItems.addAll(homeListItemsResponse.getData().getListItems());
                upDateList();
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);
            }
        });
    }

    private void upDateList() {
        if(rvListItems!=null) {
            try {
                rvListItems.setAdapter(new HomeListAdapter(listItems));
                rvListItems.getAdapter().notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
