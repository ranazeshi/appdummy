package com.darwingtechnologies.android.appname.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.utilities.CommonMethods;


/**
 * Created by Rana Zeshan on 27-Jun-20.
 */

public class EmptyFragment extends BaseFragment {
    @Override
    protected void initView() {

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

    }

    public static EmptyFragment newInstance() {
        EmptyFragment fragment = new EmptyFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.empty_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return EmptyFragment.class.getSimpleName();
    }


}
