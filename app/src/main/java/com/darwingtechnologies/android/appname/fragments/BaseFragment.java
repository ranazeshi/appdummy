package com.darwingtechnologies.android.appname.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.darwingtechnologies.android.appname.R;
import com.darwingtechnologies.android.appname.activities.MainActivity;
import com.darwingtechnologies.android.appname.fragments.dialogs.ExitDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 *
 * Created by Rana Zeshan on 27-Jun-20.
 */


@SuppressLint("ValidFragment")
public abstract class BaseFragment extends Fragment {
    protected FragmentActivity mActivity;
    protected View mView;
    private static Button btnBack;
    private static ImageButton ibBack;
    private static ImageButton ibFontSize;
    boolean doubleBackToExitPressedOnce = false;

    public void setBtnBack(Button btnBack) {
        this.btnBack = btnBack;
        ibBack = null;
    }

    public static void setIbFontSize(ImageButton ibFontSize) {
        BaseFragment.ibFontSize = ibFontSize;
    }

    public static ImageButton getIbFontSize() {
        return ibFontSize;
    }

    public void setIbBack(ImageButton ibBack, String pageName) {
    BaseFragment.ibBack = ibBack;

    if (pageName != null) {
        TextView tvPageName = mActivity.findViewById(R.id.tvPageName);

        if (tvPageName != null) {
            tvPageName.setText(pageName);
        }
    }

    ibBack.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity.instance().isShowSpinKit(false);
                    MainActivity.instance().popBackStack();
                }
            });
}

    public static Button getBtnBack() {
        return btnBack;
    }

    public static ImageButton getIbBack() {
        return ibBack;
    }

    public BaseFragment() {

    }

    abstract protected void initView();

    abstract protected void loadData();

    abstract public String getFragmentTag();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            return checkForTapAgain();
                        }
                        if (btnBack == null && ibBack == null) {
                            mActivity.getSupportFragmentManager().popBackStack();
                        } else if (btnBack != null) {
                            btnBack.performClick();
                            btnBack = null;
                        } else {
                            ibBack.performClick();
                            ibBack = null;
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        initView();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mActivity = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    boolean checkForTapAgain(){
        if (doubleBackToExitPressedOnce) {
            return false;
        } else {
//            CommonMethods.showToast("TAP again to exit.", Toast.LENGTH_SHORT);
            ExitDialogFragment infoDialogFragment = new ExitDialogFragment();
            infoDialogFragment.show(mActivity.getSupportFragmentManager(), ExitDialogFragment.class.getSimpleName());

//            this.doubleBackToExitPressedOnce = true;
            new Handler()
                    .postDelayed(
                            new Runnable() {

                                @Override
                                public void run() {
                                    doubleBackToExitPressedOnce = false;
                                }
                            },
                            2000);
            return true;
        }
    }

}
